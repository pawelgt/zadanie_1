/**
 * Created by pawel on 01.12.2015.
 */
public interface Exporter {
    Object exportObject();
}
