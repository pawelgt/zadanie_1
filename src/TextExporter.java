/**
 * Created by Pawel on 2016-01-02.
 */
public class TextExporter implements Exporter {
    Object o;

    public TextExporter(String text) {
        this.o = text;
    }

    public String exportString() {
        return (String) o;
    }

    public Object exportObject() {
        return o;
    }
}
