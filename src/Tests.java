import junit.framework.TestCase;

/**
 * Created by pawel on 01.12.2015.
 */
public class Tests extends TestCase {

    public void testExporter() throws Exception {
        String text = "Ala ma kota";
        Exporter exporter = new TextExporter(text);

        Object exportedData = exporter.exportObject();
        String exportedText = ((TextExporter) exporter).exportString();
        assertEquals(text, exportedText);
        assertEquals(text, exportedData);
    }

    public void testImporter() throws Exception {
        String text = "Ala zgubila dolara";
        Importer importer = new TextImporter();
        Object dataInImporter = text;

        importer.importData(dataInImporter);

        assertEquals(text, ((TextImporter) importer).importedText());
        assertEquals(dataInImporter, importer.importedData());
    }

    public void testFactory() throws Exception {
        String text = "Ali kot zjadl dolara";
        Factory factory = new TextFactory(text);

        Object dataFromFactory = factory.createData();
        String textFromFactory = (String) dataFromFactory;
        assertEquals(text, textFromFactory);

        Exporter exporter = factory.createExporter(text);
        String textFromExporter = ((TextExporter) exporter) .exportString();
        assertEquals(text, textFromExporter);

        Importer importer = factory.createImporter();
        assertTrue(importer instanceof Importer);

    }
}