/**
 * Created by Pawel on 2016-01-02.
 */
public class TextImporter implements Importer{
    private Object o;

    public TextImporter() {}

    public void importData(Object o) {
        this.o = o;
    }

    public String importedText() {
        return (String) o;
    }

    public Object importedData() {
        return o;
    }
}
