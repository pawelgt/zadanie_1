/**
 * Created by pawel on 01.12.2015.
 */
public interface Importer {
    void importData(Object o);
    Object importedData();
}
